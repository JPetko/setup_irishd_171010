;========= Digital Doc Autohotkey Script ==========
;
; Author: jpetko@digi-doc.com                                       
; Script purpose: Find Twain window and emulate Alt + S when button engaged.
; Date: 10/14/2017	
; Compiled Version Name: IRIS_HD_SIDEXISXG.exe   
;	
;-----------------------------------------------------------
;	File Version History:  	
;   Initial Build  -  v.01
;
;
;-----------------------------------------------------------

#NoEnv  ; Recommended for performance and compatibility with future AutoHotkey releases.
; #Warn  ; Enable warnings to assist with detecting common errors.
SendMode Input  ; Recommended for new scripts due to its superior speed and reliability.
SetWorkingDir %A_ScriptDir%  ; Ensures a consistent starting directory.
#SingleInstance ignore ;Prevents Multiple copies from running simultaneously.
RunAs, Administrator

SetKeyDelay, 500
joy1::
send !s
return
