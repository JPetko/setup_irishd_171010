﻿;========= Digital Doc Autohotkey Script ==========
;
; Author: jpetko@digi-doc.com                                       
; Script purpose: Capture Images in Mipacs Using 
; Date: 8/31/2016	
; Compiled Version Name: IRIS_HD_MIPACS.exe   
;	
;-----------------------------------------------------------
;	File Version History:  	
;   Initial Build  -  v.01
;
;-----------------------------------------------------------
#NoEnv  ; Recommended for performance and compatibility with future AutoHotkey releases.
; #Warn  ; Enable warnings to assist with detecting common errors.
SendMode Input  ; Recommended for new scripts due to its superior speed and reliability.
SetWorkingDir %A_ScriptDir%  ; Ensures a consistent starting directory.
RunAs, Administrator
#SingleInstance ignore ;Prevents Multiple copies from running simultaneously.

#IfWinExists Video frame grabber
joy1::
WinActivate Video frame grabber
send !c

