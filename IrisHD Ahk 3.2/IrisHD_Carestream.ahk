﻿;========= Digital Doc Autohotkey Script ==========
;
; Author: James Petko                                       
; Script purpose: Capture and TWAIN interface launch in Kodak Digital Imaging
; Date: 6/17/2016
; Compiled Version Name: IrisHD_Carestream.exe
;	
;-----------------------------------------------------------
;	File Version History:  	
;	0.0.1 modified 'send space' to 'send s': key send would select setup if it were selected object
;	0.0.2 added SingleInstance
;	0.0.3 increased the sleep time by 500ms
;
;
;	
;-----------------------------------------------------------
#NoEnv  ; Recommended for performance and compatibility with future AutoHotkey releases.
;#Warn  ; Enable warnings to assist with detecting common errors.
SendMode Input  ; Recommended for new scripts due to its superior speed and reliability.
SetWorkingDir %A_ScriptDir%  ; Ensures a consistent starting directory.
#SingleInstance ignore ;Prevents Multiple copies from running simultaneously.
RunAs, Administrator

;-----------------------------------------------------------

joy1::
send s
sleep (2000)
send !f
send n
send a