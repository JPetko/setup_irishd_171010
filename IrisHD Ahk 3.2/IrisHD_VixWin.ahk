;========= Digital Doc Autohotkey Script ==========
;
; Author: jpetko@digi-doc.com                                       
; Script purpose: Find Twain window and emulate Alt + S when button engaged.
; Date: 8/31/2016	
; Compiled Version Name: IRIS_HD_VIXWIN.exe   
;	
;-----------------------------------------------------------
;	File Version History:  	
;   Initial Build  -  v.01
;	Added relaunch for Vixwin Twain
;	Added the Squelching for the errors that are generated in vixwin
;	Increased the delay timer to give camera enough time to close previous stream before reinit.
;-----------------------------------------------------------

#NoEnv  ; Recommended for performance and compatibility with future AutoHotkey releases.
; #Warn  ; Enable warnings to assist with detecting common errors.
SendMode Input  ; Recommended for new scripts due to its superior speed and reliability.
SetWorkingDir %A_ScriptDir%  ; Ensures a consistent starting directory.
#SingleInstance ignore ;Prevents Multiple copies from running simultaneously.
RunAs, Administrator

joy1::
WinActivate HDC1330 TWAIN Image Capture
send !s
Sleep, 2500
send !fo
return