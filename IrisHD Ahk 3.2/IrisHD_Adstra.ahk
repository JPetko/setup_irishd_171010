﻿;========= Digital Doc Autohotkey Script ==========
;
; Author: jpetko@digi-doc.com                                       
; Script purpose: Find Twain window and emulate Alt + S when button engaged.
; Date: 8/31/2016 | v02 05/21/2017 | v03 10/29/2017	
; Compiled Version Name: IrisHD_Adstra.exe   
;	
;-----------------------------------------------------------
;	File Version History:  	
;   Initial Build  -  v.01
;	Method Revision - v.02 {Redesigned Moving mouse to Scan button}
;	Added the WinActivate function to ensure the Twain Window is active when keystroke is sent.
;	Added RunAs function to execute with elevated permissions.
;	v.03 - changed SingleInstance from force to ignore, to avoid permissions needing to be added every launch.
;-----------------------------------------------------------

#NoEnv  ; Recommended for performance and compatibility with future AutoHotkey releases.
; #Warn  ; Enable warnings to assist with detecting common errors.
SendMode Input  ; Recommended for new scripts due to its superior speed and reliability.
SetWorkingDir %A_ScriptDir%  ; Ensures a consistent starting directory.
#SingleInstance ignore ; Prevents multiple hotkeys running simultaneously


joy1::
WinActivate HDC1330 TWAIN Image Capture
send !s