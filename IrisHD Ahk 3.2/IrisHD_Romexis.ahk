﻿;========= Digital Doc Autohotkey Script ==========
;
; Author: jpetko@digi-doc.com                                       
; Script purpose: Capture images in Romexis software by simulating keystrokes
; Date: 8/31/2016	
; Compiled Version Name: IrisHD_Romexis.exe   
;	
;-----------------------------------------------------------
;	File Version History:  	
;   Initial Build  -  v.01
;	Method Revision - v.02
;-----------------------------------------------------------

#NoEnv  ; Recommended for performance and compatibility with future AutoHotkey releases.
; #Warn  ; Enable warnings to assist with detecting common errors.
SendMode Input  ; Recommended for new scripts due to its superior speed and reliability.
SetWorkingDir %A_ScriptDir%  ; Ensures a consistent starting directory.
#SingleInstance ignore ;Prevents Multiple copies from running simultaneously.
RunAs, Administrator

state=0
joy1::
if (state = 0){
state = 1
send !f
}
else{
state=0
send !r
}
return

joy2::
send !s

