﻿;========= Digital Doc Autohotkey Script ==========
;
; Author: jpetko@digi-doc.com                                       
; Script purpose: Find Twain window and emulate Alt + S when button engaged.
; Date: 8/31/2016	
; Compiled Version Name: IrisHD_CurveHero.exe   
;	
;-----------------------------------------------------------
;	File Version History:  	
;   Initial Build  -  v.01
;	v.02 - Added WinActivate for CurveHero Window, in some cases it was being moved to the back.
;-----------------------------------------------------------
#NoEnv  ; Recommended for performance and compatibility with future AutoHotkey releases.
; #Warn  ; Enable warnings to assist with detecting common errors.
SendMode Input  ; Recommended for new scripts due to its superior speed and reliability.
SetWorkingDir %A_ScriptDir%  ; Ensures a consistent starting directory.
RunAs, Administrator
#SingleInstance ignore

#IfWinExists HDC1330 TWAIN Image Capture
joy1::
WinActivate, HDC1330 TWAIN Image Capture
send !s
Sleep, 1000
WinActivate, Curve Hero