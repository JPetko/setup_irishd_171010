﻿;========= Digital Doc Autohotkey Script ==========
;
; Author:jpetko@digi-doc.com                                       
; Script purpose: Capture images in Schick CDR version 5 by sending keystrokes
; Date: 10/29/2017
; Compiled Version Name: IrisHD_Schick CDR English.exe  
;	
;-----------------------------------------------------------
;	File Version History:  	
;	v01- initial build
;	
;-----------------------------------------------------------
#NoEnv  ; Recommended for performance and compatibility with future AutoHotkey releases.
; #Warn  ; Enable warnings to assist with detecting common errors.
SendMode Input  ; Recommended for new scripts due to its superior speed and reliability.
SetWorkingDir %A_ScriptDir%  ; Ensures a consistent starting directory.
#SingleInstance ignore ;Prevents Multiple copies from running simultaneously.
RunAs, Administrator

joy1::
WinActivate, HDC1330 TWAIN Image Capture
send {space}
sleep, 3000
send {enter}
sleep, 1000
