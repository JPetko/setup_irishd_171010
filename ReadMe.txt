Project Name:
Setup_IrisHD_171010

Language:
Inno Setup Script (.iss)

Description:
Places driver files in correct directories for install. Includes IrisHD_Install_Assistant to set resolution and Dental Imaging
Software prefs. Application will also launch applicable AutoHotkey Scripts if needed. Creates start menu entry for Digital Doc
weblinks and utilities for user. there is an additional zip file at the install Directory that contains advanced troubleshooting
applications and methods for interaction with the Iris HD Firmware.

How to Use:
User sets install directory, and will be prompted in the Iris Install Assistant to select resolution and dental software.

Author: 
James Petko (petko.james@gmail.com)

Date:
10/29/2017